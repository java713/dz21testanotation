package org.example;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        MyClass myClass = new MyClass();
        myClass.Test(1, 2);

        final Class<?> cls = myClass.getClass();
        Method[] methods = cls.getMethods();
        for (Method method : methods){
            if (method.isAnnotationPresent(Test.class)) {
                Test an = method.getAnnotation(Test.class);
                try {
                    method.invoke(myClass,an.a(), an.b());
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                } catch (InvocationTargetException e) {
                    throw new RuntimeException(e);
                }
            }

        }

    }
}
