package org.example;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(value = ElementType.METHOD)
@Retention(value = RUNTIME)
@interface Test {
    int a() default 2;
    int b() default 5;

}
